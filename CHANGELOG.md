CHANGELOG
=========

1.0.0 (2015-11-19)
------------------

* rewrote package to work with Guzzle v6

0.2.1 (2015-06-17)
------------------

* bug fix - missing use clause for exception

0.2.0 (2015-06-17)
------------------

* refactored to split Guzzle calls into separate class so we can provide a simpler mailing list interface

0.1.0 (2015-06-17)
------------------

* initial release
